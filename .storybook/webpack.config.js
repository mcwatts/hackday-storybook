const path = require('path')

const sassVariablesPath = "src/style/variables.scss";

module.exports = async ({ config }) => {
  config.resolve.alias['~storybook'] = path.resolve(__dirname)

  config.module.rules.push({
    resourceQuery: /blockType=story/,
    loader: 'vue-storybook',
  })

  config.module.rules.push({
    test: /\.s(a|c)ss$/,
    use: ['style-loader', 'css-loader',             {
                loader: "sass-loader",
                options: {
                    prependData: `@import '${sassVariablesPath}';`,
                },
            },],
    include: path.resolve(__dirname, '../'),
  })

  config.module.rules.push({
        test: /\.ts(x?)$/,
        use: [
            {
                loader: "ts-loader",
                options: {
                    transpileOnly: true,
                },
            },
        ],
    })

  return config
}

// const loaders = [
//     {
//         test: /\.ts(x?)$/,
//         use: [
//             {
//                 loader: "ts-loader",
//                 options: {
//                     transpileOnly: true,
//                 },
//             },
//         ],
//     },
//     {
//         test: [/\.vue\.scss$/, /style.scss$/, /\.css$/],
//         use: [
//             "style-loader",
//             "css-loader",
//             {
//                 loader: "sass-loader",
//                 options: {
//                     prependData: `@import '${sassVariablesPath}';`,
//                 },
//             },
//         ],
//     },
//     {
//         test: /\.scss$/,
//         exclude: [/\.vue\.scss$/, /style.scss$/],
//         loaders: ["ignore-loader"],
//     },
//     {
//         test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
//         loader: "url-loader?limit=10000&mimetype=application/font-woff",
//     },
//     {
//         test: /\.(ttf|eot|jpe?g|png|gif|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
//         loader: "file-loader",
//     },
//     {
//         test: /\.html$/,
//         loader: "html-loader",
//         options: {
//             ignoreCustomFragments: [/\{\{.*?}}/],
//             attrs: ["img:src", "link:href"],
//         },
//     },
//     // {
//     //     test: /\.json$/,
//     //     loader: "json-loader",
//     // },
//     {
//         test: /\.vue$/,
//         loader: "vue-loader",
//     },
// ]