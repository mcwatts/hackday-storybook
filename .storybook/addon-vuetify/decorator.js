// Imports
import Vue from 'vue'
import Vuetify from 'vuetify'
import "materialize-css/dist/js/materialize.min";
import { makeDecorator } from '@storybook/addons';

// Utilities
import deepmerge from 'deepmerge'

// Vuetify
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.min.css'

Vue.use(Vuetify)

export default makeDecorator({
  name: 'withVuetify',
  parameterName: 'vuetify',
  wrapper: (storyFn, context, { parameters = {} }) => {
    const WrappedComponent = storyFn(context)

    return Vue.extend({
      components: { WrappedComponent },
      template: `
        <v-app>
          <v-container fluid>
            <wrapped-component />
          </v-container>
        </v-app>
      `,
    })
  },
})
