import { text, boolean } from '@storybook/addon-knobs'
import Vue from "vue";
import Avatar from "../../src/components/generic/avatar.vue";

// register the component so the stories can render it
Vue.component('avatar', Avatar);

export default { title: 'Avatar Vue Component'}

// write your stories!
export const userAvatar = () => `<avatar size="50">
<img src="../images/avatar.png"/>
</avatar>`;
export const clientAvatar = () => `<avatar tile size="76"><img src="../images/avatar.jpg"/></avatar>`
export const fallbackAvatar = () => `<avatar>A</avatar>`;
