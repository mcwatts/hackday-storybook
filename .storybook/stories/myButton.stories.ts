// Utilities
import { text, boolean } from '@storybook/addon-knobs'
import Vue from "vue";
import MyButton from "../../src/components/MyButton.vue";

// register the component so the stories can render it
Vue.component('my-button', MyButton);

// give the story a title
export default { title: 'Example TS Vue Component'}

// write your stories!
export const withText = () => "<my-button>with texty</my-button>"
export const withEmojis = () => "<my-button>🤪</my-button>"
export const vuetifyComponents = () => `<v-card>
          <v-img
            class="white--text"
            height="200px"
            src="https://cdn.vuetifyjs.com/images/cards/docks.jpg"
          >
            <v-container fill-height fluid>
              <v-layout fill-height>
                <v-flex xs12 align-end flexbox>
                  <span class="headline">Top 10 Australian beaches</span>
                </v-flex>
              </v-layout>
            </v-container>
          </v-img>
          <v-card-title>
            <div>
              <span class="grey--text">Number 10</span><br>
              <span>Whitehaven Beach</span><br>
              <span>Whitsunday Island, Whitsunday Islands</span>
            </div>
          </v-card-title>
          <v-card-actions>
            <v-btn flat color="orange">Share</v-btn>
            <v-btn flat color="orange">Explore</v-btn>
          </v-card-actions>
        </v-card>`