// Utilities
import { withKnobs, text, number, boolean, select } from '@storybook/addon-knobs'
import Vue from "vue";
import MessageBar from "../../src/components/generic/messagebar/messagebar.vue";

// register the component so the stories can render it
Vue.component('messagebar', MessageBar);

enum MessagebarType {
    error = "error",
    warning = "warning",
    warningPersist = "warningPersist",
    lease = "lease",
    request = "request",
    info = "info",
    infoPersist = "infoPersist",
    success = "success",
    edit = "edit",
    save = "save",
}
// give the story a title
export default { title: 'Messagebar', decorators: [withKnobs] }

export const info = () => `<div class="messagebar-container"><messagebar msg="A message in the bar" type="${MessagebarType.info}" /></div>`;
export const error = () => `<div class="messagebar-container"><messagebar msg="An error has occurred" type="${MessagebarType.error}" /></div>`;
export const withCloseButton = () => `
    <div class="messagebar-container">
        <messagebar msg="An error has occurred" type="${MessagebarType.error}" method="close"/>
    </div>`
export const withCustomButton = () => `
    <div class="messagebar-container">
        <messagebar msg="A custom button text 👀" type="${MessagebarType.success}" method="close" button="Custom!"/>
    </div>`
export const exampleWithKnobs = () => ({
  components: { MessageBar },
  props: {
    type: {
      default: select('Type', Object.values(MessagebarType), MessagebarType.error)
    },
    msg: {
      default: text('Text', 'Hello Storybook')
    },
    method: {
        default: select("Method", ["", "close"], "")
      },
      buttonText: {
          default: text('Button Text', "")
    }
  },
    template: `
    <div class="messagebar-container">
        <messagebar :msg="msg" :type="type" :method="method" :button="buttonText"/>
    </div>`
});