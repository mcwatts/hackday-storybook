// Utilities
import { text, boolean, withKnobs } from '@storybook/addon-knobs'
import Vue from "vue";
import FilterButton from "../../src/components/generic/filter-button.vue";
import FilterSelect from "../../src/components/generic/filter-select.vue";
import FilterButtonGroup from "../../src/components/generic/filter-button-group.vue";

// register the component so the stories can render it
Vue.component('filter-button', FilterButton);
Vue.component('filter-select', FilterSelect);
Vue.component('filter-button-group', FilterButtonGroup);

// give the story a title
export default { title: 'Filter Button Stories'}

// write your stories!
export const Active = () => ({
    component: {
        FilterButton
    },
    props: {
        text: {
            type: String,
            default: text("text", "Hello Amads")
        },
        disabled: {
            type: Boolean,
            default: boolean("disabled", false)
        },
        isActive: {
            type: Boolean,
            default: boolean("isActive", true)
        },
        round: {
            type: Boolean,
            default: boolean("round", false)
        },
        small: {
            type: Boolean,
            default: boolean("small", false)
        },
        icon: {
            type: Boolean,
            default: boolean("icon", false)
        },
        separator: {
            type: Boolean,
            default: boolean("separator", false)
        },
        solidBg: {
            type: Boolean,
            default: boolean("solidBg", false)
        }
    },
    template: `<filter-button
        :text="text"
        :seperator="separator"
        :solidBg="solidBg"
        :icon="icon" 
        :disabled="disabled"
        :isActive="isActive"
        :round="round"
        :small="small"
    ></filter-button>`,
    decorators: [withKnobs]
})

export const Disabled = () => ({
    component: {
        FilterButton
    },
    props: {
        text: {
            type: String,
            default: text("text", "Hello Amads")
        },
        disabled: {
            type: Boolean,
            default: boolean("disabled", true)
        },
        isActive: {
            type: Boolean,
            default: boolean("isActive", false)
        },
        round: {
            type: Boolean,
            default: boolean("round", false)
        },
        small: {
            type: Boolean,
            default: boolean("small", false)
        },
        icon: {
            type: Boolean,
            default: boolean("icon", false)
        },
        separator: {
            type: Boolean,
            default: boolean("separator", false)
        },
        solidBg: {
            type: Boolean,
            default: boolean("solidBg", false)
        }
    },
    template: `<filter-button
        :text="text"
        :seperator="separator"
        :solidBg="solidBg"
        :icon="icon" 
        :disabled="disabled"
        :isActive="isActive"
        :round="round"
        :small="small"
    ></filter-button>`,
    decorators: [withKnobs]
})

export const singleSelect = () => `<filter-select
                    filter="option a"
                    :filter-options="['option a', 'option b']"
                    :disabled="false"
                />`

export const groupWithSeparators = () => `
                <filter-button-group>
                <filter-select
                    :separator="true"
                    filter="one"
                    :filter-options="['one', 'two', 'three']"
                />
                <filter-button
                    :separator="true"
                    text="Bookmarked"
                    :is-active="false"
                    icon="fa-bookmark"
                />
                <filter-button
                    text="Save"
                    :is-active="false"
                    icon="fa-save"
                />
            </filter-button-group>
`