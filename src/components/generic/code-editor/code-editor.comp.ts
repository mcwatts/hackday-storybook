import Vue from "vue";
import Component from "vue-class-component";
import { Prop, Watch } from "vue-property-decorator";
import "./_code-editor.vue.scss";

@Component({
    template: require("./code-editor.vue.html")
})
export default class CodeEditor extends Vue {
    @Prop(String) readonly value: string;

    private code: string = null;

    public get codeTemplate(): string {
        return this.code;
    }

    public set codeTemplate(value: string) {
        this.code = value;
    }

    @Watch("code")
    public onCodeTemplateChange(value: string): void {
        this.$emit("input", value);
    }

    private mounted(): void {
        this.code = this.value;
    }
}