import "reflect-metadata";
import Vue from "vue";
import Component from "vue-class-component";
import "./_deletebutton.vue.scss";

const template: string = "<div class='delete-button' :class='{confirm : confirming}'>" +
                            "<div class='delete-button-content'>" +
                                "<span class='delete-button-text' @click='handleClick'><slot></slot></span>" +
                                "<div class='confirm-buttons'>" +
                                    "Are you sure?" +
                                    "<i class='confirm-yes fa fa-2x fa-check' @click='deleteClicked'></i>" +
                                    "<i class='confirm-no fa fa-2x fa-times' @click='confirming = false'></i>" +
                                "</div>" +
                            "</div>" +
                        "</div>";

@Component({
    template,
    props: ["text"]
})
export class DeleteButton extends Vue {
    private confirming = false;

    private handleClick() {
        this.confirming = true;
    }
    
    private deleteClicked() {
        this.confirming = false;
        this.$emit("click");
    }
}