import Component, { mixins } from "vue-class-component";
import HeaderMixin from "../mixins/header.mixin";
import "./_header.vue.scss";
import { Prop } from "vue-property-decorator";

@Component({
    template: require("./header.vue.html")
})
export default class Header extends mixins(HeaderMixin) {
    @Prop(Boolean) readonly asterix: boolean;
}