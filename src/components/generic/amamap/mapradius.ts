export class MapRadius {
    constructor(
        latlngString: string | string[],
        radiusStringNumber: string | string[]
    ) {
        this.latlong = latlngString.toString();
        this.radius = (parseFloat(radiusStringNumber?.toString()) * 1000) || 0;
        this.center = {
            lat: parseFloat(latlngString.toString().split(",")[0]),
            lng: parseFloat(latlngString.toString().split(",")[1])
        };
        this.options = new RadiusOptions({
            strokeColor: "#03a9f4",
            strokeOpacity: 0.6,
            strokeWeight: 2,
            draggable: true,
            geodesic: true,
            fillColor: "#03a9f4",
            fillOpacity: 0.1
        });
    }
    public id: string;
    public latlong: string;
    public radius: number;
    public center: {
        lat: number;
        lng: number;
    };
    public newSelected: boolean;
    public options: RadiusOptions;
    public strokeColor: string;
    public fillColor: string;
    private _selected: boolean;
    private _selectedColor: string = "#E91E63";
    private defaultColor: string = "#03a9f4";

    public static readonly highlights = {
        yellow: "#FFB300"
    };

    public get selected(): boolean {
        return this._selected;
    }

    public set selected(val) {
        this._selected = val;
        if (val) {
            this.options.fillColor = this.selectedColor;
            this.options.strokeColor = this.selectedColor;
        } else {
            this.options.fillColor = this.defaultColor;
            this.options.strokeColor = this.defaultColor;
        }
    }

    public get selectedColor(): string {
        return this._selectedColor;
    }

    public set selectedColor(val: string) {
        this._selectedColor = val;
    }
    

}

class RadiusOptions {
    private _fillColor: string;
    private _strokeColor: string;
    get strokeColor(): string {
        return this._strokeColor;
    }
    set strokeColor(val: string) {
        this._strokeColor = val;
    }
    get fillColor(): string {
        return this._fillColor;
    }
    set fillColor(val: string) {
        this._fillColor = val;
    }
    public strokeOpacity: number;
    public strokeWeight: number;
    public draggable: boolean;
    public geodesic: boolean;
    public fillOpacity: number;

    constructor(data: IRadiusOptions) {
        Object.assign(this, data);
    }
}

interface IRadiusOptions {
    strokeColor: string;
    strokeOpacity: number;
    strokeWeight: number;
    draggable: boolean;
    geodesic: boolean;
    fillColor: string;
    fillOpacity: number;
}