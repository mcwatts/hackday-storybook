import "reflect-metadata";
import Vue from "vue";
import Component from "vue-class-component";
import { Prop } from "vue-property-decorator";

@Component({
    template: require("./date.vue.html")
})

export class DateComponent extends Vue {
    @Prop() public date:string;

    public get nicedate() {
        const date = new Date(this.date);
        const month = new Intl.DateTimeFormat("en-GB", { month: "short" }).format(date);
        const day = date.getDate();
        let dayString = day.toString();
        if (day < 10) {
            dayString = "0"+day;
        }
        return {year: date.getFullYear(), month, day: dayString};
    }
    
    public get shortYear() {
        const date = new Date(this.date);
        return new Intl.DateTimeFormat("en-GB", { year: "2-digit" }).format(date);
    }
}