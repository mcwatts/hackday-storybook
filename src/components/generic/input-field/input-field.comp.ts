import Component, { mixins } from "vue-class-component";
import HeaderComp from "../header/header.comp";
import HeaderMixin from "../mixins/header.mixin";
import "./_input-field.vue.scss";
import { Prop } from "vue-property-decorator";

@Component({
    template: require("./input-field.vue.html"),
    components: {
        HeaderComp
    }
})
export default class InputField extends mixins(HeaderMixin) {
    @Prop(Boolean) readonly required: boolean;
}