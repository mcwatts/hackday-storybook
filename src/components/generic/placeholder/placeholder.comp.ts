import Vue from "vue";
import { Prop, Component } from "vue-property-decorator";
import "./_placeholder.vue.scss";

@Component({
    template: require("./placeholder.vue.html")
})
export default class Placeholder extends Vue {
    @Prop({type: String, required: true}) readonly text: string;
    @Prop(String) readonly icon: string;
    @Prop(Number) readonly height: number;
}