import "reflect-metadata";
import Vue from "vue";
import Component from "vue-class-component";
import "./_list.limiter.vue.scss";
import { Prop } from "vue-property-decorator";
import { IConditionFilter } from "@app/components/scriptbuilder/grid/grid.comp";

@Component({
    template: require("./list.limiter.vue.html"),
}) 
export default class ListLimiter extends Vue {
    @Prop() public searching: boolean;
    @Prop() public filterConditions: IConditionFilter[];
    @Prop() public noRules: boolean;
    @Prop() public itemCount: number;
    @Prop() public totalItemCount: number;
    @Prop() public limit: number;
    @Prop() public origLimit: number;
    @Prop() public toggleMode: boolean;
    @Prop() public collapseMode: boolean;
    @Prop() public redirect: string;
    @Prop() public redirectText: string;
    @Prop() public redirectIcon: string;

    public increaseLimit() {
        this.$emit("increaselimit");
    }

    public delimit() {
        this.$emit("delimit");
    }

    public resetLimit() {
        this.$emit("resetlimit");
    }

    public resetFilters(resetSearch?: boolean, resetConditionFilters?: boolean) {
        this.$emit("resetfilters", { resetSearch, resetConditionFilters });
    }

    public get filterDefault():boolean {
        return this.$store.state.scriptbuilder.findDefault;
    }

    public get filterErrors():boolean {
        return this.$store.state.scriptbuilder.findErrors;
    }
}