import Vue from "vue";
import Component from "vue-class-component";
import { Prop, Watch } from "vue-property-decorator";
import Cropper from "cropperjs/dist/cropper.esm.js";
import "cropperjs/dist/cropper.css";
import "./_image-upload.vue.scss";

@Component({
    template: require("./image-upload.vue.html")
})
export default class ImageUpload extends Vue {
    @Prop(String) readonly logoUrl: string;
    @Prop(Boolean) readonly imageUploading: boolean;
    @Prop(Boolean) readonly saving: boolean;

    public cropping: boolean = false;
    public editableLogoUrl: string = null;

    private cropper: Cropper;
    private fileToUpload: File = null;

    private crop: {
        height:number,
        width:number,
        x:number,
        y:number
    };

    @Watch("logoUrl")
    public logoUrlChange(logoUrl: string): void {
        this.editableLogoUrl = logoUrl ? `${logoUrl}?v=${Math.floor((Math.random() * 100)).toString()}` : null;
    }

    @Watch("cropping")
    public onCroppingChange(cropping: boolean): void {
        this.$emit("on-cropping", cropping);

        if (!cropping && this.fileToUpload) {
            this.$emit("image-chosen", {file: this.fileToUpload, fileUrl: this.editableLogoUrl, crop: this.crop});
        }
    }

    public removeImage(): void {
        this.editableLogoUrl = null;
        this.$emit("image-removed");
    }

    public finishCrop(accept: boolean): void {
        this.cropping = false;

        if (accept) {
            this.crop = this.cropper.getData();
            this.editableLogoUrl = this.cropper.getCroppedCanvas().toDataURL();
        } else {
            this.editableLogoUrl = URL.createObjectURL(this.fileToUpload);
        }

        this.cropper.destroy();
    }

    public upload(event: any): void {
        const image = document.getElementById("logo-preview");

        this.editableLogoUrl = URL.createObjectURL(event.target.files[0]);
        this.fileToUpload = event.target.files[0];
        this.cropping = true;

        this.cropper = new Cropper(image,{
            dragMode: "move",
            guides: false,
            highlight: true,                    
            responsive: false,
            modal: false,
            autoCropArea: 1,
            scalable: true,
            zoomable: true,
            minContainerWidth: 200,
            minContainerHeight: 200,
            background: false
        });

        this.cropper.replace(this.editableLogoUrl);
    }

    private mounted(): void {
        this.logoUrlChange(this.logoUrl);
    }
}