import "reflect-metadata";
import Vue from "vue";
import "./hamburger.vue.scss";
import { Component, Prop } from "vue-property-decorator";

@Component({
    template: `
        <div class="hamburger-btn right" :class="{'light-blue darken-3' : menuExpanded }" @click="clickMenuIcon">
            <div class="hamburger-btn-inner" :class="{'active' : menuExpanded}"></div>
        </div>`
})
export class Hamburger extends Vue {
    @Prop() public menuExpanded: boolean;

    public clickMenuIcon() {
        this.$emit("click");
    }
}
