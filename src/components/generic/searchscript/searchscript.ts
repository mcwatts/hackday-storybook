import Vue from "vue";
import Component from "vue-class-component";
import { AmadsApi, Script } from "amads-common";
import { Container } from "typedi";
import "./_searchscript.vue.scss";
import { Watch, Prop } from "vue-property-decorator";

@Component({
    template: `
        <v-autocomplete
            :menu-props="{contentClass:'search-script'}"
            v-model="model"
            :items="items"
            :loading="isLoading"
            :search-input.sync="search"
            color="light-blue"
            item-text="scriptname"
            item-value="id"
            clearable
            no-filter
            hide-selected
            hide-no-data
            :hide-details="!search || (search && search.length > 2)"
            hint="Please enter at least 3 characters"
            :label="label"
            :placeholder="hidePlaceholder ? null : placeholder || 'Search...'"
            @keyup="handleSearch"
            return-object
        >
            <template v-slot:item="{ item }">
                <v-list-tile-content>
                    <v-list-tile-title>
                        <span v-if="item.version" class="script-version--search mr-1">v{{ item.version }}</span>
                        <i
                            v-if="item.parent.scriptname"
                            class="fa fa-code-fork"
                            style="width: 8px; margin-left: 1px;"
                        ></i>
                        {{ item.scriptname }}
                    </v-list-tile-title>
                    <v-list-tile-sub-title>
                        <span v-if="item.parent.scriptname">
                            <i class="fa fa-volume-up"></i>
                            {{ item.parent.scriptname }}
                        </span>
                        <span v-else-if="item.type === 'branch'">
                            (
                            <i class="fa fa-code-fork ml-0"></i>
                            Branched)
                        </span>
                        <span v-if="item.campaign && item.campaign_id">
                            <i class="fa fa-bullhorn"></i>
                            {{ item.campaign.campaign }}
                        </span>
                        <span v-else-if="item.campaign_id === 0">
                            <i class="fa fa-align-left"></i>
                            My Workspace
                        </span>
                    </v-list-tile-sub-title>
                </v-list-tile-content>
            </template>
            <template v-slot:selection="{ item }">
                <span class="selected-script">
                    <span v-if="item.version" class="script-version--search mr-1">v{{ item.version }}</span>
                    {{ item.scriptname }}
                </span>
            </template>
        </v-autocomplete>
    `,
})
export class SearchScript extends Vue {
    public api: AmadsApi = Container.get("amadsApi");
    public items: Script[] = [];
    public isLoading = false;
    public search = null;
    public model: Script = null;

    @Prop() public label: string;
    @Prop() public value: Script;
    @Prop() public placeholder: string;
    @Prop() public hidePlaceholder: boolean;
    @Prop() public showbranched: boolean;
    @Prop() public mediatype: string;
    @Prop() public omitId: string;

    @Watch("showbranched")
    @Watch("mediatype")
    private resetItems(): void {
        this.items = [];
    }

    @Watch("model")
    private modelChanged(): void {
        this.$emit("input", this.model);
        this.search = null;
    }

    @Watch("value")
    private valueChanged(): void {
        this.model = this.value;
    }

    private handleSearch(): void {
        if (this.search && this.search.length > 2) {
            this.items = [];
            this.isLoading = true;
            this.api.script
                .query(this.search)
                .then((list) => {
                    const withText = list
                        .filter((item) => !this.mediatype || item.mediatype === this.mediatype)
                        .filter(
                            (item) =>
                                this.showbranched === undefined || this.showbranched === true || item.type !== "branch"
                        ) // either we are allowing branch parents, or this is a single script
                        .filter((item) => !this.omitId || item.uid !== this.omitId); // this is the item that we don't want to include
                    this.items.push(...withText);
                    this.isLoading = false;
                    const caret = this.$el.getElementsByClassName("v-input__icon--append").item(0) as HTMLElement;
                    if (caret) {
                        caret.style.visibility = null;
                    }
                    return list;
                })
                .catch((err) => {
                    console.error(err);
                    this.items = [];
                    this.isLoading = false;
                });
        } else {
            this.items = [];
            this.$emit("input", null);
        }
    }

    private mounted(): void {
        const caret = this.$el.getElementsByClassName("v-input__icon--append").item(0) as HTMLElement;
        if (caret) {
            caret.style.visibility = "hidden";
        }
    }
}
