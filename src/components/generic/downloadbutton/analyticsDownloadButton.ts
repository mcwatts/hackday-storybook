import "reflect-metadata";
import Vue from "vue";
import Component from "vue-class-component";

const app = angular.module("madsang2");

@Component({
    template: `<div class="analyticsAnnouncement"></div>`
})
export default class AnalyticsDownloadButton extends Vue {

}

app.directive("analyticsDownloadButton", ["createVueComponent", (createVueComponent) => {
    return createVueComponent(AnalyticsDownloadButton);
}]);