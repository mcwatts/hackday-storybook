import Vue from "vue";
import { Dictionary } from "vue-router/types/router";
import Component from "vue-class-component";

@Component
export class RouteParamsMixin<T extends Dictionary<string>> extends Vue {
    public get routeParams(): T {
        return this.$route.params as T;
    }
}
