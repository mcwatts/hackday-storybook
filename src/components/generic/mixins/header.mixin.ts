import Vue from "vue";
import Component from "vue-class-component";
import { Prop } from "vue-property-decorator";

@Component
export default class HeaderMixin extends Vue {
    @Prop({type: String, required: true}) readonly title: string;
    @Prop(String) readonly icon: string;
    @Prop(String) readonly fontSize: string;
    @Prop(String) readonly color: string;
    @Prop(String) readonly colorVariant: string;
    @Prop(String) readonly fontWeight: string;
}