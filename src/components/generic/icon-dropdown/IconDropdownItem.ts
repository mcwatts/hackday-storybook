import { Rule, RuleType } from "amads-common";

export interface IconDropdownItem {
    name?: string;
    desc: string;
    value: any;
    itemClass?: string;
    tooltipClass?: string;
    disabled?: boolean;
    icon?: string;
    superIcon?: string;
    validator?: (...args: any[]) => boolean;
}

export interface RotationTypeItem extends IconDropdownItem {
    validator: (currentRule: Rule, ruleTypes: { [index: string]: RuleType }) => boolean;
}