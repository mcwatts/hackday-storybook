import Vue from "vue";
import Component from "vue-class-component";
import { Prop } from "vue-property-decorator";
import "./_list.vue.scss";

@Component({
    template: require("./list.vue.html")
})
export default class ListComp extends Vue {
    @Prop(Array) readonly items: Array<any>;
    @Prop(Boolean) readonly loading: boolean;
    @Prop(String) readonly title: string;
    @Prop(String) readonly name: string;
    @Prop(Boolean) readonly resourceIcon: boolean;
    @Prop(String) readonly itemIcon: string;
    @Prop(Boolean) readonly showAddNewAction: boolean;
    @Prop(Function) readonly addNewActionCallback: () => void;
    @Prop(Function) readonly editCallback: (entity: any) => void;

    public icons: {[key: string]: string} = {
        user: "fa-user",
        script: "fa-volume-up"
    };

    public get titleLower(): string {
        return this.title.toLowerCase();
    }
}