import "reflect-metadata";
import Vue from "vue";
import Component from "vue-class-component";
import moment from "moment";
import { Prop, Watch } from "vue-property-decorator";
import "./_datetime.vue.scss";

@Component({
    template: `
        <span class="date-time-component">
            <v-menu
                ref="dialog"
                content-class="date-picker--dropdown"
                v-model="datePickerOpen"
                :disabled="disabled"
                persistent
                lazy
                width="336px"
                :close-on-content-click="false"
                transition="scale-transition"
            >
                <template v-slot:activator="{ on }">
                    <v-text-field
                        class="datetime__text-input"
                        v-on="on"
                        v-model="dateDisplay"
                        :placeholder="placeholder"
                        hide-details
                        readonly
                    >
                        <template v-slot:label><slot name="label"></slot></template>
                    </v-text-field>
                </template>
                <v-date-picker v-model="dateValue" color="light-blue" scrollable :min="minDate" :max="maxDate">
                    <v-spacer></v-spacer>
                    <v-content v-if="editTime">
                        <v-layout class="xs12 datetime__time">
                            <v-text-field
                                hide-details
                                :placeholder="'HH'"
                                min="00"
                                max="23"
                                label="hour"
                                type="number"
                                mask="##"
                                :value="hours"
                                @change="v=>hours=v"
                            ></v-text-field>
                            &nbsp;:&nbsp;
                            <v-text-field
                                hide-details
                                :placeholder="'MM'"
                                min="00"
                                max="60"
                                label="mins"
                                type="number"
                                mask="##"
                                :value="minutes"
                                @change="v=>minutes=v"
                            ></v-text-field>
                        </v-layout>
                    </v-content>
                    <v-layout class="xs12">
                        <v-spacer></v-spacer>
                        <v-btn flat color="primary" @click="clear()">Clear</v-btn>
                        <v-btn flat color="primary" @click="close()">OK</v-btn>
                    </v-layout>
                </v-date-picker>
            </v-menu>
        </span>
    `,
})
export class DateTimeControl extends Vue {
    @Prop({ default: null }) value: string; // ISO formatted date

    @Prop({ default: false }) disabled: boolean;

    @Prop({ default: "Set date" }) placeholder: string;

    @Prop({ default: null }) min: string;

    @Prop({ default: null }) max: string;

    @Prop({ default: null }) defaultTime: string; // e.g. 00:00 (start time) or 23:59 (end time)

    @Prop({ default: true }) editTime: boolean;

    @Prop() label: string;

    public dateTime: string = null; // ISO formatted date
    public datePickerOpen = false;

    @Watch("datePickerOpen")
    public datePickerOpenChanged(): void {
        if (!this.datePickerOpen) {
            return;
        }

        let value;
        if (!this.value) {
            const defaultTimeMoment = this.defaultTime ? moment(this.defaultTime, "HH:mm") : null;
            const min = this.min ? moment(this.min).utc() : null;
            const max = this.maxDate ? moment(this.maxDate).utc() : null;
            value = moment().utc(); // current local time

            if (this.min) {
                // e.g. start date/time
                if (min.isAfter(value)) {
                    // start date/time is after the current/default date/time: set end to same as start
                    value = min.add(1, "minutes"); // put value one minute after minimum
                }
            }

            if (this.maxDate) {
                // e.g. end date/time
                if (max.isBefore(value)) {
                    // end date/time is before the current/default date/time: set start to same as end
                    value = max.subtract(1, "minutes"); // put value one minute before maximum
                }
            }

            if (defaultTimeMoment) {
                // set value to default time if given and valid, using date from above
                if (
                    !this.min ||
                    defaultTimeMoment.hours() > min.hours() ||
                    !this.maxDate ||
                    defaultTimeMoment.hours() < max.hours()
                ) {
                    value = this.setTime(value, defaultTimeMoment);
                }
            }
        } else {
            value = moment(this.value).utc();
        }

        value.seconds(0);
        this.dateTime = value.format();
    }

    private setTime(value: moment.Moment, defaultTimeMoment: moment.Moment): moment.Moment {
        value.hours(defaultTimeMoment.hours());
        value.minutes(defaultTimeMoment.minutes());
        return value;
    }

    public get minDate(): string {
        if (this.min) {
            return moment(this.min).utc().subtract(1, "day").format();
        } else {
            return null;
        }
    }

    public get maxDate(): string {
        return this.max;
    }

    public get dateDisplay(): string {
        return this.value ? moment(this.value).utc().format("lll") : null;
    }
    public get dateValue(): string {
        return this.dateTime ? moment(this.dateTime).utc().format("YYYY-MM-DD") : null;
    }

    public set dateValue(newDate) {
        const dateTime = moment.utc(newDate, "YYYY-MM-DD");
        if (dateTime.isValid()) {
            const time = moment.utc(this.dateTime);
            if (time.isValid()) {
                dateTime.hours(time.hours());
                dateTime.minutes(time.minutes());
            } else {
                dateTime.hours(0);
                dateTime.minutes(0);
            }
            this.dateTime = dateTime.utc().format();
        }
    }

    public get hours(): string {
        return this.dateTime ? moment.utc(this.dateTime).format("HH") : null;
    }

    public set hours(hour) {
        const time = moment.utc(hour, "HH");

        let dateTime = moment.utc(this.dateTime);
        if (time.isValid()) {
            dateTime.hours(time.hours());
            if (dateTime.isBefore(moment.utc(this.min))) {
                dateTime = moment.utc(this.min);
            }
            if (dateTime.isAfter(moment.utc(this.max))) {
                dateTime = moment.utc(this.max);
            }
        } else {
            dateTime.hours(0);
        }
        this.dateTime = dateTime.utc().format();
    }

    public get minutes(): string {
        return this.dateTime ? moment.utc(this.dateTime).format("mm") : null;
    }

    public set minutes(minutes) {
        const time = moment.utc(minutes, "mm");
        let dateTime = moment.utc(this.dateTime);

        if (time.isValid()) {
            dateTime.minutes(time.minutes());
            if (dateTime.isBefore(moment.utc(this.min))) {
                dateTime = moment(this.min).utc();
            }
            if (dateTime.isAfter(moment.utc(this.max))) {
                dateTime = moment.utc(this.max);
            }
        } else {
            dateTime.minutes(0);
        }
        this.dateTime = dateTime.utc().format();
    }

    public clear(): void {
        this.dateTime = null;
        this.close();
    }

    public close(): void {
        this.change();
        this.datePickerOpen = false;
        this.dateTime = null;
    }

    private change(): void {
        this.$emit("input", this.dateTime);
    }
}
