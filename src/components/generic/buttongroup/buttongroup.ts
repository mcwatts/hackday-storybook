import Vue from "vue";
import Component from "vue-class-component";
import {mapState} from "vuex";
import "./_buttongroup.vue.scss";
import {GroupButton} from "../buttongroup/groupbutton";

@Component({
  template: "<div class='button-group'><slot></slot></div>",
  props: ["exclusive"],
  components : {GroupButton}
})

export class ButtonGroup extends Vue {
  public selectedButtonComp:any = null;
  
  private created() {
    this.$on("buttonselected", (button) => {
      this.handleClick(button);
    });
    this.$on("buttonsetup", (button) => {
      this.handleClick(button, true); // runs on load by button that has "selected" prop
    });
  }
  
  private handleClick(button, setup?:boolean) {
    if (this.$props.exclusive) { // we can only select one of the group at a time
      if (button !== this.selectedButtonComp) { // a different button was clicked - this runs for all setups
        if (this.selectedButtonComp) {
          this.selectedButtonComp.selectButton(false); // deselect the old button
        }
        button.selectButton(true); // select new button
        this.selectedButtonComp = button; // store new button to use in next click
      }
    } else { // we can select multiple buttons at once
      button.selectButton(setup);
      // (de)select new button. setup is either undefined (to swap state) or contains true (to select the button)
    }
  }
}