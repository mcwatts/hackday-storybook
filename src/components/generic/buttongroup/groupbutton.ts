import Vue from "vue";
import Component from "vue-class-component";

@Component({
  template: require("./groupbutton.html"),
  props: ["text","selected"],
  methods: {
    onClick() {
      this.$parent.$emit("buttonselected",this); // passes button up to parent group
    }
  },
  data: () => {
    return {buttonSelected: this.$props.selected};
  }
})

export class GroupButton extends Vue {
  private mounted() {
    if (this.$props.selected) {
      this.$parent.$emit("buttonsetup",this); // passes button up to parent group
    } 
  }

  private selectButton(selecting?:boolean) {
    if (selecting === undefined) { // swap selected state
      this.$data.buttonSelected = !this.$data.buttonSelected;
    } else { // set to specific state
      this.$data.buttonSelected = selecting;
    }
  }
}