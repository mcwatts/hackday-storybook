import "reflect-metadata";
import Vue from "vue";
import Component from "vue-class-component";
import "./_iconbutton.vue.scss";

const template: string =    `<div class='icon-button'
                                :class='[direction, {"no-arrow":useArrow===false}]'
                                @click='handleClick'>
                                <slot></slot>
                                <div class='arrow' v-if="useArrow">
                                    <i class='fa fa-2x'
                                    :class='direction === "left" ? "fa-caret-left" : "fa-caret-right"'
                                    ></i>
                                </div>
                            </div>`;

@Component({
    template,
    props: ["direction", "noArrow"],
    data: () => {
        return {
            useArrow: true
        };
    }
})
export class IconButton extends Vue {
    private handleClick() {
        this.$emit("click");
    }

    private created() {
        if (this.$props.noArrow !== undefined) {
            this.$data.useArrow = false;
        }
    }
}